\subsection{Cadre de travail et méthodologie}
\label{sub:moi:cadre}

% \subsubsection{Bien être au travail}
% \label{ssub:moi:cadre:bienetre}
%
% \textcolor{red}{useful ?}
% \textcolor{red}{Startup palace, devfest, web2day, NCraft}

\subsubsection{Méthodes Agiles}
\label{ssub:moi:cadre:agile}

Chez Dictanova nous utilisons, pour le travail de l'équipe technique, les méthodes Agiles et en particulier un mix des méthodes \emph{\gls{scrum}} et \emph{\gls{kanban}}. \\

Les méthodes Agiles sont des pratiques aujourd'hui très répandues, voir courantes, dans les équipes de développement logiciel. Elles permettent, contrairement à des méthodes plus traditionnelles, d'avoir une plus grande flexibilité et réactivité à la demande, pour permettre un développement plus centré sur le besoin exact et avoir, au final, un produit qui correspond parfaitement aux attentes.

Néanmoins, les méthodes Agiles ne sont pas toujours mises en place de la même façon, ni à la même échelle dans les entreprises. \\

Nous sommes en perpétuel changement et amélioration de notre mode de travail. Aussi, nous avons choisir d'unir des caractéristiques des deux grandes méthodes Agiles \emph{\gls{scrum}} et \emph{\gls{kanban}} pour faire du “Scrumban”. \\

Nous découpons donc nos périodes de développement en “\emph{sprint}”. Ces \emph{sprint} sont des périodes de 3 semaines. \\

Avant chaque période de \emph{sprint} nous structurons et priorisons les demandes afin d'en sélectionner certaines sous la forme de \emph{stories}. De plus, nous respectons l'ensemble des phases de la méthode \emph{\gls{scrum}}. Néanmoins, nous utilisons un tableau \emph{\gls{kanban}}, et il se peut que des demandes extérieures soient ajoutées à tout moment dans le sprint en cours. \\

\begin{figure}[H]
  \centering
  \includegraphics[width=.6\textwidth]{src/assets/sprint}
  \caption{Étapes clés d'un sprint}
  \label{fig:sprint}
\end{figure}

Chaque \emph{sprint} est précédé d'un point (regroupant tous les membres de l'équipe technique) appelé \emph{sprint planning}. C'est dans ce point que nous sélectionnons les tâches qui vont être faites dans le \emph{sprint} à partir du \emph{product backlog}. Cette sélection se fait selon plusieurs critères : la difficulté de la tâche, son importance en terme de stratégie commerciale, d'acquisition et/ou de conservation de client, et la vision produit détenue par le \emph{\gls{po}}, ici Antoine (responsable produit). Néanmoins, il est important de souligner que nous faisons pas nécessairement tout ce qui ce qui est demandé par les clients. Il est capital de savoir sélectionner en terme de valeur une fonctionnalité ou une idée. \\

Contrairement à ce que préconise \emph{\gls{scrum}}, nous ne faisons pas d’estimation de nos tâches en \emph{Story points}. Nous avons essayé à de nombreuses reprises de faire une estimation préalable des tâches avec différents modes d'évaluation, mais les résultats ont rarement été concluants. Notre estimation est donc très approximative, basée sur un mode du niveau de connaissance de la faisabilité : on sait faire (on a déjà fait), on pense savoir comment faire (on a des idées), on n'a jamais fait (inconnu, phase de recherche). \\

La particularité de notre travail est que celui-ci est souvent très prospectif. Tout est toujours nouveau. En effet, on détecte un besoin chez nos utilisateurs mais on ne sait pas forcément comment totalement y répondre (les utilisateurs ne savent souvent pas non plus ce qui pourrait les aider). Cela est dû au côté nouveau du besoin pour lequel Dictanova travaille : l'analyse sémantique des retours clients. Rien ou peu de choses existent, et il faut tout créer, inventer et innover. \\ 

Chaque \emph{sprint} est également succédé d'un point appelé \emph{rétrospective} avec l'ensemble des collaborateurs ayant participé au \emph{sprint}. Dans ce créneau différentes “activités” sont faites. Tout d'abord, on regarde l'avancement de chaque tâche prévue : celles terminées, celles qui non pas été terminées ou pas commencées. On fait un point sur notre efficacité et les points d'amélioration possibles. Enfin, chacun fait un bilan personnel de son \emph{sprint} : comment il l'a vécu, quel problème il a rencontré, qu'est-ce qui pourrait l'aider lors du prochain \emph{sprint}. Le but de ce point est de tirer des enseignements et d'améliorer constamment notre manière de travailler. \\

Comme évoqué dans la section~\ref{ssub:dictanova:organisation:rdv} concernant les rendez-vous hebdomadaires, nous faisons quotidiennement un \emph{stand-up meeting}. Cette pratique, toujours issue des pratiques de développement Agile et \emph{\gls{scrum}} en particulier, consiste en une réunion d'une quinzaine de minutes, se faisant debout, au cours de laquelle chaque membre de l'équipe expose le travail qu'il a effectué la veille, les difficultés qu'il a rencontré, ainsi que les tâches qu’il compte réaliser le jour même.

Ces réunions permettent à chacun de prendre du recul sur ce qu’il a accompli, afin de pouvoir l’exposer de manière claire et synthétique aux autres membres de l’équipe. Dans mon cas, ces exercices m’ont incité à améliorer mon esprit de synthèse, et ma capacité à exposer de manière “non centrée sur le code” les missions sur lesquelles je travaille. \\ 

\subsubsection{Intégration continue}
\label{ssub:moi:cadre:intecontinue}

La qualité de notre travail et de ce qu'on produit est un critère très important chez Dictanova. Outre notre organisation en terme de gestion de projet, nous utilisons, d'un point de vu plus technique pour cela, l'intégration continue avec \emph{\gls{jenkins}}. \\

L'intégration continue est un ensemble de pratiques consistant à vérifier à chaque modification du code source que le résultat des modifications ne produit pas de régression dans l'application développée. Plus concrètement, à chaque fois qu'un développeur envoie ses modifications sur le serveur stockant les dépôts de code source, l'ensemble des tests de l'application sont lancés sur ces modifications afin de voir que tout fonctionne toujours correctement (dans le cas contraire un mail lui est envoyé et son travail est mis en mode “échec”). Le principal but de cette pratique est de détecter les problèmes d'intégration au plus tôt lors du développement. De plus, elle permet d'automatiser l'exécution des suites de tests et de voir l'évolution du développement du logiciel. \\

Mais l'intégration continue et les bonnes pratiques qui en découlent ne s'arrêtent pas à cela. L'intégration continue nous permet également d'automatiser les compilations, de maintenir un cycle de compilation court, de tester dans un environnement de production cloné (appelé \emph{staging} chez nous), d'avoir une visibilité sur le projet pour tous les membres de l'équipe, de rendre disponible facilement le dernier exécutable, dans le but d'automatiser le déploiement. Concernant le déploiement, du fait de l'évolution rapide et constante du produit, nous mettons en production au minimum une fois par semaine. \\

\subsubsection{Tests}
\label{ssub:moi:cadre:tests}

Chez Dictanova, nous prenons une grande importance à proposer un produit et un travail de qualité. En développement logiciel cela passe notamment par une phase de test. Je présenterai plus en détail les différents types de tests et leur mise en place dans notre équipe technique dans la section~\ref{sub:travaux:e2e} et je vais me concentrer sur le côté méthodologie du test dans cette partie. \\

Il existe plusieurs stratégies dans le développement et la rédaction de tests. Chez Dictanova nous en utilisons deux en particulier : le TDD et depuis peu le BDD. \\

Le \textbf{TDD} (Test Driver Development) consiste à développer (mettre en place) les tests avant de développer la fonctionnalité. Les tests, très spécifiques et couvrant une large possibilités de cas, sont donc au départ en échec (comportement attendu). Le but de l'opération est donc de faire passer tous ces tests en succès. Lorsque ceux-ci sont en succès, on est quasiment sûr que la fonctionnalité développée répond aux attentes.

Le \textbf{BDD} (Behaviour Driven Development) est une stratégie différente. C'est une pratique permettant de déterminer les comportements d'un produit dans un langage interprété. Plus précisément, il s'agit de rédiger des scénarios en langue naturelle qui pourront être lancés automatiquement et utilisés en tant que tests. On part alors des comportements fonctionnels. \\

Ces deux méthodes ne sont pas incompatibles, bien au contraire. Le BDD ajoute simplement au TDD plus d'expression du besoin en langage naturel pour garantir la qualité fonctionnelle. Le TDD ne garantissant d'une certaine façon que la qualité technique d'une implémentation.

\subsubsection{Environnement technique}
\label{ssub:moi:cadre:technique}

Notre environnement technique est assez complet. Je travaille sous Mac comme la majorité des salariés chez nous.

Nous utilisons en grande partie la suite \emph{Atlassian}. Elle permet de répondre à de nombreux besoins.

Parmi les outils utilisés dans cette suite on retrouve :

\begin{itemize}
  \item \emph{Hipchat} qui nous sert de messagerie instantanée en interne
  \item \emph{Jira} pour le suivi de projet et de tickets ainsi que pour la remontée de bug
  \item Nous utilisons comme gestionnaire de version de code \gls{git} à travers la solution \emph{Bitbucket Server}
  \item \emph{Confluence} comme logiciel de wiki où nous écrivons toutes les documentations mais aussi tous nos documents internes \\ 
\end{itemize}

Nous utilisons également le logiciel \emph{Aha} pour la gestion de la \gls{roadmap}, et ElCurator pour l’échange d’articles divers intéressants entre nous.

Enfin nous utilisons majoritairement \emph{Docker} comme solution de \gls{containerisation}. \\ 

Concernant la plateforme Dictanova nous utilisons plusieurs outils tel que \emph{Mixpanel} pour le tracking des actions utilisateur, \emph{Logmatic} comme système de centralisation des \glspl{log}. Pour tout ce qui concerne le support client nous utilisons \emph{Intercom}. Cette solution permet au client de nous envoyer un message (lorsque celui-ci rencontre un problème ou se pose une question) directement depuis la plateforme. Tous collaborateurs chez Dictanova (en particulier l'équipe technique et les Customer Success Manager) ont la possibilité d'y répondre sous un système de chat in-app.  \\

\textbf{AngularJS} \\

La plateforme Dictanova est une application réalisée avec \emph{AngularJS}. \\ 

AngularJS est un \gls{framework} JavaScript libre et open-source développé par \emph{Google}.

Le \gls{framework} est fondé sur l'idée que la \emph{\gls{progdeclarative}} doit être utilisée pour construire les interfaces utilisateur et les composants logiciels, tandis que la \emph{\gls{progimperative}} correspond parfaitement pour exprimer la logique métier. \\

Il se base sur l'adaptation et l'extension du langage \emph{\gls{html}} (par ajout de nouvelles balises) et sur un certain nombre de patrons et principes de conception, en particulier le patron logiciel \gls{mvc} afin d'encourager le couplage faible entre la présentation, les données, et les composants métier.

Le code HTML étendu représente alors la partie “vue” du patron de conception \gls{mvc} auquel AngularJS adhère, avec des modèles appelés “scopes” et des contrôleurs permettant de définir des actions. AngularJS utilise une boucle de \emph{dirty-checking} (qui consiste à surveiller et à détecter des modifications sur un objet JavaScript) pour réaliser un \emph{data-binding} bidirectionnel permettant la synchronisation automatique des modèles et des vues. \\

Les avantages du \gls{framework} sont divers :

\begin{itemize}
  \item Séparation des manipulations du \gls{dom} et de la logique métier
  \item Forte testabilité des composants, il est important de considérer le test de l'application aussi important que l'écriture de l'application elle-même
  \item Découper les parties client (front-end) et serveur (back-end) d'une application, permettant au développement logiciel des deux parties d'avancer en parallèle et permettant la réutilisabilité de chacun (une limite que de nombreux \glspl{framework} largement utilisés ont aujourd'hui)
  \item Un système d'injection de dépendances efficace permettant à l'application web côté client les services traditionnellement présents côté serveur
  \item La prise en charge des \glspl{promesse} (promises), un des fondements même du fonctionnement \gls{asynchrone} du \gls{framework} \\
\end{itemize}

D'un point de vue global, le \gls{framework} tend à rendre les tâches faciles évidentes et les tâches difficiles possibles. \\

Parmi les bibliothèques JavaScript que nous utilisons, nous retrouvons en grande partie \emph{Lodash}. Cette bibliothèque fournit un ensemble de fonctions utilitaires pour des tâches de programmation courantes (en particulier pour la manipulation et transformation de collections de données) en utilisant le paradigme de \emph{\gls{progfonctionnelle}}.
